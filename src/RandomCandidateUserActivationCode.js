import {ApplicationService, Guid} from '@themost/common';
class RandomCandidateUserActivationCode extends ApplicationService {
    constructor(app) {
        super(app);
    }
    /**
     * 
     * @param {DataContext} context 
     * @param {*} candidate 
     * @returns 
     */
    // eslint-disable-next-line no-unused-vars
    async generate(context, candidate) {
        return Guid.newGuid().toString().substr(24);
    }

}

export {
    RandomCandidateUserActivationCode
};
