import { DataObjectState } from "@themost/data";
import { DataNotFoundError } from "@themost/common";

/**
 * @param {DataEventArgs} event
 */
 async function beforeSaveAsync(event) {
    // // get context
    // const context = event.model.context;
    // if (event.state === DataObjectState.Insert) {
    //     // get candidate department
    //     let candidateDepartment, searchProperty;
    //     if (event.target.department.alternativeCode) {
    //         // ensure that alternativeCode is passed as string
    //         event.target.department.alternativeCode = event.target.department.alternativeCode.toString();
    //         candidateDepartment = event.target.department.alternativeCode;
    //         searchProperty = 'department/alternativeCode';
    //     } else {
    //         candidateDepartment = event.target.department.id || event.target.department;
    //         searchProperty = 'department';
    //     }
    //     // get the department's last active study program
    //     const lastActiveSP = await context.model('StudyProgram')
    //         .where(searchProperty).equal(candidateDepartment.toString())
    //         .and('isActive').equal(1)
    //         .expand('specialties')
    //         .orderByDescending('id')
    //         .getItem();
    //     if (lastActiveSP == null) {
    //         throw new DataNotFoundError('The candidate study program cannot be found or is inaccessible');
    //     }
    //     // get the core specialty of the study program
    //     const studyProgramSpecialty = lastActiveSP.specialties.find(sPSpecialty => sPSpecialty.specialty === -1);
    //     if (studyProgramSpecialty == null) {
    //         throw new DataNotFoundError('The candidate study program specialty cannot be found or is inaccessible');
    //     }
    //     // define extra required fields
    //     const extraRequiredFields = {
    //         studyProgram: lastActiveSP.id,
    //         studyProgramSpecialty: studyProgramSpecialty.id,
    //         specialtyId: -1
    //     };
    //     // assign fields to candidate student
    //     Object.assign(event.target, extraRequiredFields);
    // }
}

/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    // // get context 
    // const context = event.model.context;
    // // create a potential StudyProgramRegisterAction
    // const studyProgramRegisterAction = {
    //     actionStatus : {
    //         alternateName : 'PotentialActionStatus'
    //     },
    //     studyProgram: event.target.studyProgram,
    //     specialization: event.target.studyProgramSpecialty,
    //     inscriptionYear: event.target.inscriptionYear,
    //     inscriptionPeriod: event.target.inscriptionPeriod,
    //     candidate: event.target,
    //     owner: ...
    // }
    // // save studyProgramRegisterAction
    // await context.model('StudyProgramRegisterAction').save(studyProgramRegisterAction);
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
 export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}
