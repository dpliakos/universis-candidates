import { ApplicationService } from '@themost/common';
import { DataEventArgs, DataContext } from '@themost/data';

export declare function afterSave(event: DataEventArgs, callback: (err?: Error) => void);

export declare class SendSmsAfterCreateCandidateUser extends ApplicationService {
    install(): void;
    send(context: DataContext, candidate: {id: number}): Promise<any>;
}