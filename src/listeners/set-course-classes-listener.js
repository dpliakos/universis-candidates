
/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    if (Object.prototype.hasOwnProperty.call(event.target, 'courseRegistrations')) {
        /**
         * @type {StudyProgramRegisterAction}
         */
        const target = event.model.convert(event.target);
        await target.setCourseRegistrations(target.courseRegistrations);
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}