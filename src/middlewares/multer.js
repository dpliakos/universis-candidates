import multer from "multer";

/**
 * Returns an instance of multer that is configured based on application configuration
 * @param {ConfigurationBase} configuration
 * @returns {multer.Instance}
 */
function multerInstance(configuration) {
    // set user storage from configuration or use default content folder content/user
    const userContentRoot = configuration.getSourceAt('settings/universis/content/user') || 'content/user';
    return multer({ dest: userContentRoot });
}

export {multerInstance}
