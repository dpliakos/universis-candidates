import { DataObject } from "@themost/data";
import { EdmMapping, EdmType , PermissionMask, DataPermissionEventListener} from "@themost/data";
import {xlsPostParserWithConfig} from '../middlewares';
const path = require('path');
import _ from 'lodash';
// eslint-disable-next-line no-unused-vars
import { DataError, DataNotFoundError } from "@themost/common";
import { TraceUtils } from "@themost/common";
import {promisify} from 'util';
import { HttpForbiddenError } from "@themost/common";
import { RandomCandidateUserActivationCode } from '../RandomCandidateUserActivationCode';
import { SendSmsAfterCreateCandidateUser } from '../listeners/send-sms-after-create-candidate-user-action';
class CandidateStudent extends DataObject {

    @EdmMapping.param('schema', EdmType.EdmStream, false)
    @EdmMapping.param('file', EdmType.EdmStream, false)
    @EdmMapping.action('import', 'CandidateStudentUploadAction')
    static async importFile(context, file, schema) {
        const req = {
            files: {
                file: [Object.assign(file, {
                    mimetype: file.contentType,
                    destination: path.dirname(file.path),
                    filename: path.basename(file.path),
                    originalfilename: file.contentFileName,
                    originalname: file.contentFileName
                })],
                schema: [Object.assign(schema, {
                    mimetype: schema.contentType,
                    destination: path.dirname(schema.path),
                    filename: path.basename(schema.path),
                    originalfilename: schema.contentFileName,
                    originalname: schema.contentFileName
                })]
            }
        }
        const res = {};
        await new Promise((resolve, reject) => {
            xlsPostParserWithConfig({
                name: "file",
                schema: "schema"
            })(req, res, (err) => {
                if (err) {
                    return reject(err);
                }
                // get body
                return resolve();
            });
        });
        if (req.body.length > 0) {
            const testDepartmentAltCode = req.body[0].department.hasOwnProperty('alternativeCode');
            const groupByExp = testDepartmentAltCode ? 'department.alternativeCode' : 'department';
            const searchPropertyDP = testDepartmentAltCode ? 'alternativeCode' : 'id';
            const searchPropertySP = testDepartmentAltCode ? 'department/alternativeCode' : 'department';
            const inscriptionModes = new Map();
            const studyPrograms = new Map();
            const lastActiveStudyPrograms = new Map();
            // group candidates by department
            const candidates = _.groupBy(req.body, groupByExp);
            // iterate through each one of the unique departments
            for (const [department, candidateList] of Object.entries(candidates)) {
                if (department === 'undefined' /* groupBy shows undefined like a string */) {
                    throw new DataError('Invalid candidates file data. Department column cannot be empty at this context.');
                }
                // validate department
                const departmentObject = await context.model('Department')
                        .where(searchPropertyDP).equal(department.toString())
                        .getItem();
                if (departmentObject == null) {
                    throw new DataNotFoundError(`The department with id or alternativeCode ${department} cannot be found or is inaccessible.`);
                }
                // validate data for each candidate
                for (const candidate of candidateList) {
                    // get inscription mode id
                    const inscriptionModeId = candidate.inscriptionMode;
                    if (inscriptionModeId) {
                        // check map to prevent uneccessary calls
                        if (!inscriptionModes.has(inscriptionModeId)) {
                            // validate inscription mode
                            const inscriptionMode = await context.model('InscriptionMode')
                                .where('id').equal(inscriptionModeId)
                                .getItem();
                            if (inscriptionMode == null) {
                                throw new DataNotFoundError(`The inscription mode with id ${inscriptionModeId} cannot be found or is inaccessible.`);
                            }
                            // cache inscription mode
                            inscriptionModes.set(inscriptionModeId, inscriptionMode);
                        }
                    } else {
                        throw new DataError('Invalid cadidate student data. Inscription mode column cannot be empty at this context.');
                    }
                    // get studyProgramId
                    const studyProgramId = candidate.studyProgram;
                    if (studyProgramId) {
                        // check map to prevent uneccessary calls
                        if (!studyPrograms.has(studyProgramId)) {
                            // validate study program
                            const studyProgram = await context.model('StudyProgram')
                                .where('id').equal(studyProgramId)
                                .and('isActive').equal(1)
                                .getItem();
                            if (studyProgram == null) {
                                throw new DataNotFoundError(`The study program with id ${studyProgramId} cannot be found, is inaccessible or is inactive.`);
                            }
                            // cache study program
                            studyPrograms.set(studyProgramId, studyProgram);
                        }
                    } else /*if study program is null, assign last active for that department*/ {
                        let lastActiveSP;
                        if (!lastActiveStudyPrograms.has(department)) {
                            // get the department's last active study program
                            lastActiveSP = await context.model('StudyProgram')
                                .where(searchPropertySP).equal(department.toString())
                                .and('isActive').equal(1)
                                .expand('specialties')
                                .orderByDescending('id')
                                .getItem();
                            if (lastActiveSP == null) {
                                throw new DataNotFoundError('The candidate study program cannot be found or is inaccessible.');
                            }
                            lastActiveStudyPrograms.set(department, lastActiveSP);
                        } else {
                            lastActiveSP = lastActiveStudyPrograms.get(department);
                        }
                        // get the core specialty of the study program
                        const studyProgramSpecialty = lastActiveSP.specialties.find(sPSpecialty => sPSpecialty.specialty === -1);
                        if (studyProgramSpecialty == null) {
                            throw new DataNotFoundError('The candidate study program specialty cannot be found or is inaccessible.');
                        }
                        // define extra required fields
                        const extraRequiredFields = {
                            studyProgram: lastActiveSP.id,
                            studyProgramSpecialty: studyProgramSpecialty.id,
                            specialtyId: -1
                        };
                        // assign fields to candidate student
                        Object.assign(candidate, extraRequiredFields);
                    }
                    // ensure that department alternative code is a string
                    if (candidate.department.alternativeCode) {
                        candidate.department.alternativeCode = candidate.department.alternativeCode.toString();
                    }
                }
            }
            const action = await context.model('CandidateStudentUploadAction').silent().save({totalCandidates: req.body.length})
            await CandidateStudent.saveCandidates(context, req.body, action);
            return action;
        } else {
            throw new DataError('No candidate students found in provided file.');
        }
    }

    static saveCandidates(appContext, candidates, action) {
        const app = appContext.getApplication();
        const context = app.createContext();
        context.user = appContext.user;
        const errors = [];
        (async function () {
            for (const candidate of candidates) {
                try {
                    await new Promise((resolve, reject) => {
                        context.model('CandidateStudent').save(candidate).then(() => {
                            return resolve();
                        }).catch(err => {
                            err.message = err.message + ` For candidate student with inscription number ${candidate.inscriptionNumber}`
                            errors.push(err.message);
                            return resolve();
                        });
                    });
                } catch (err) {
                    TraceUtils.error(err);
                }
            }
        })().then(() => {
            // finalize context
            context.finalize(() => {
                action.message = errors.length > 0 ? errors.join(' --- ') : null; 
                action.actionStatus = {alternateName: 'CompletedActionStatus'};
                action.endTime = new Date();
                return context.model('CandidateStudentUploadAction').silent().save(action).then(action => {
                }).catch(err => {
                    TraceUtils.error(err);
                });
            });
        }).catch(err => {
            context.finalize(() => {
                action.actionStatus = {alternateName: 'FailedActionStatus'};
                action.endTime = new Date();
                action.message = err.message;
                return context.model('CandidateStudentUploadAction').silent().save(action).then(action => {
                }).catch(err => {
                    TraceUtils.error(err);
                });
            });
        });
    }

    /**
     * Create candidate user
     */
    @EdmMapping.action('createUser', 'CandidateStudent')
    async createUser() {
        const model = this.context.model('CandidateStudent');
        // infer object state
        const candidate = await model.where('id').equal(this.getId()).expand('person','user').getItem();
        if (candidate == null) {
            return;
        }
        // check if user already set
        if (candidate.user) {
            return candidate;
        }
        const username = candidate.inscriptionNumber.toString() || candidate.person.email;
        // check if user already exists
        const exists = await this.context.model('User').where('name').equal(username).silent().count();
        if (exists)
        {
            throw new Error(`User ${username} already exists`)
        }
        // create user
        const user = {
            "name": username,
            "email": candidate.person.email,
            "description":`${candidate.person.familyName} ${candidate.person.givenName}`
        };

        const service = this.context.getApplication().getService(RandomCandidateUserActivationCode);
        if (service != null) {
            const activationCode = await service.generate(this.context, this);
            Object.assign(user, {
                activationCode
            });
        }
        const result = await new Promise((resolve, reject) => {
            this.context.db.executeInTransaction((cb) => {
                return this.context.model('CandidateUser').silent().save(user).then((updated) => {
                    candidate.user = updated;
                    return model.save(candidate).then(() => {
                        return cb();
                    });
                }).catch((err) => {
                    return cb(err);
                });
            }, (err) => {
                if (err) {
                    return reject(err);
                }
                return resolve(candidate);
            });
        })
        return result;
    }

    @EdmMapping.action('enroll', 'StudyProgramRegisterAction')
    async createStudyProgramRegisterAction() {
        const model = this.context.model('CandidateStudent');
        // get candidate
        const candidate = await model.where('id').equal(this.getId()).getItem();
        if (candidate == null) {
            throw new DataNotFoundError(`The candidate student cannot be found or is inaccessible.`);
        }
        /* check if candidate user exists
        if (!candidate.user) {
            return;
        }*/
        // check if action already exists for specific candidate
        const existingAction = await this.context.model('StudyProgramRegisterAction').where('candidate').equal(candidate.id).getItem();
        if (existingAction) {
            return existingAction;
        }
        // create studyProgramRegisterAction
        const studyProgramRegisterAction = {
            studyProgram: candidate.studyProgram,
            specialization: candidate.studyProgramSpecialty,
            inscriptionYear: candidate.inscriptionYear,
            inscriptionPeriod: candidate.inscriptionPeriod,
            inscriptionMode: candidate.inscriptionMode,
            candidate: candidate.id,
            owner: candidate.user // if owner is null, the studyProgramRegisterAction validation listener will throw an error
        };
        // save studyProgramRegisterAction
        return this.context.model('StudyProgramRegisterAction').save(studyProgramRegisterAction);
    }

    @EdmMapping.action('sendActivationMessage', 'CandidateStudent') 
    async sendActivationMessage()
    {
        // get validator listener
        const validator = new DataPermissionEventListener();
        // noinspection JSUnresolvedFunction
        const validateAsync = promisify(validator.validate)
            .bind(validator);
        // validate CandidateStudent/SendActivationMessage execute permission
        const event = {
            model: this.getModel(),
            privilege: 'CandidateStudent/SendActivationMessage',
            mask: PermissionMask.Execute,
            target: this,
            throwError: false
        }
        await validateAsync(event);
        if (event.result === false) {
            throw new HttpForbiddenError();
        }
        /**
         * @type {ApplicationBase}
         */
        const app = this.context.getApplication();
        let service = app.getService(SendSmsAfterCreateCandidateUser);
        if (service != null) {
            return await service.send(this.context, this);
        }
        // otherwise check if application has sms service enabled
        const parentService = app.getService(function SmsService() {});
        if (parentService == null) {
            throw new Error('The operation cannot be completed due to invalid application configuration. A required service is missing');
        }
        service = new SendSmsAfterCreateCandidateUser(app);
        return await service.send(this.context, this);
    }
}

module.exports = CandidateStudent;
